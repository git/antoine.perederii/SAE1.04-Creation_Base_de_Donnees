DROP TABLE RETOUR_R;
DROP TABLE RETOUR_L;
DROP TABLE CONTIENT;
DROP TABLE AVOIR;
DROP TABLE POSSEDE;
DROP TABLE RESERVER;
DROP TABLE LOUER;
DROP TABLE LIVRAISON;
DROP TABLE FOURNISSEUR;
DROP TABLE CLIENT;
DROP TABLE ZONE_GEOGRAPHIQUE;
DROP TABLE RESERVATION;
DROP TABLE LOCATION;
DROP TABLE ETAT_PROD;
DROP TABLE PRODUIT;
DROP TABLE TYPES;

CREATE TABLE FOURNISSEUR(
    id_fournisseur          char(4) PRIMARY KEY,
    nom varchar(30)         UNIQUE NOT NULL,
    note numeric(5)         NOT NULL CHECK(note >= 0 AND note <= 5),
    description             varchar(50)
);

CREATE TABLE LIVRAISON (
    id_livraison            char(4) PRIMARY KEY,
    date_livraison          date NOT NULL,
    id_fournisseur          char(4) REFERENCES FOURNISSEUR(id_fournisseur)
);

CREATE TABLE TYPES (
    id_type                 char(4) PRIMARY KEY,
    description             varchar(50) UNIQUE NOT NULL
);

CREATE TABLE PRODUIT (
    id_produit              char(4) PRIMARY KEY, 
    nom                     varchar(50) UNIQUE NOT NULL,
    qte                     numeric NOT NULL CHECK(qte >= 0),
    prix                    numeric(6,2) NOT NULL CHECK(prix > 0),
    id_type                 char(4) REFERENCES TYPES(id_type)
);

CREATE TABLE POSSEDE (
    qte_livree              numeric NOT NULL CHECK(qte_livree > 0),
    id_produit              char(4) REFERENCES PRODUIT(id_produit),
    id_livraison            char(4) REFERENCES LIVRAISON(id_livraison)
);

CREATE TABLE RESERVATION (
    id_reservation          char(4) PRIMARY KEY,
    date_reservation        date NOT NULL,
    date_deb                date NOT NULL,
    CHECK(date_reservation != date_deb)
);

CREATE TABLE CONTIENT (

    qte                     numeric NOT NULL CHECK(qte > 0),
    id_produit              char(4) REFERENCES PRODUIT(id_produit),
    id_reservation          char(4) REFERENCES RESERVATION(id_reservation)
);

CREATE TABLE ETAT_PROD (
    etat_prod               char(4) PRIMARY KEY,
    caution                 numeric NOT NULL CHECK (caution > 0)
);

CREATE TABLE LOCATION (
    id_location             char(4) PRIMARY KEY,
    nom                     varchar(30) NOT NULL,
    note                    numeric NOT NULL CHECK (note >= 0 AND note <= 5),
    description             varchar(50),
    date_deb                date NOT NULL,
    etat_prod               char(4) REFERENCES ETAT_PROD(etat_prod)
);

CREATE TABLE AVOIR (
    qte                     numeric NOT NULL CHECK(qte > 0),
    id_produit              char(4) REFERENCES PRODUIT(id_produit),
    id_location             char(4) REFERENCES LOCATION(id_location)
);

CREATE TABLE ZONE_GEOGRAPHIQUE (
    num_region              char(4) PRIMARY KEY,
    region                  varchar(30) UNIQUE NOT NULL
);

CREATE TABLE CLIENT (
    id_client               char(4) PRIMARY KEY,
    nom                     varchar(30) NOT NULL,
    num_tel                 numeric(10) UNIQUE NOT NULL CHECK(num_tel > 0 AND num_tel < 0999999999),
    num_rue                 numeric(3) NOT NULL,
    rue                     varchar(50) NOT NULL,
    code_postal             char(5) NOT NULL,
    ville                   varchar(30) NOT NULL,
    type_client             varchar(30) NOT NULL,
    authorize_reduc         char(1) NOT NULL CHECK(authorize_reduc = 'y' OR authorize_reduc ='n'),
    num_region              char(4) REFERENCES ZONE_GEOGRAPHIQUE(num_region),
    CHECK(NOT(authorize_reduc = 'y' AND type_client = 'particulier')),
    CHECK(NOT(authorize_reduc = 'n' AND type_client = 'professionnelle'))
);

CREATE TABLE RESERVER(
    id_client               char(4) REFERENCES CLIENT(id_client),
    id_reservation          char(4) REFERENCES RESERVATION(id_reservation)
);

CREATE TABLE LOUER(
    id_client               char(4) REFERENCES CLIENT(id_client),
    id_location             char(4) REFERENCES LOCATION(id_location)
);

CREATE TABLE RETOUR_L (
    date_retour             date NOT NULL,
    etat_produit            char(4) NOT NULL CHECK(etat_produit IN ('E001', 'E002', 'E003')),
    id_location             char(4) REFERENCES LOCATION(id_location),
    id_client               char(4) REFERENCES CLIENT(id_client)
);

CREATE TABLE RETOUR_R (
    date_retour             date NOT NULL,
    id_reservation          char(4) REFERENCES RESERVATION(id_reservation),
    id_client               char(4) REFERENCES CLIENT(id_client)
);

